﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
namespace Client
{
    class Program
    {
      static  Socket socket;
        static void Main(string[] args)
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8000);
            try
            {
                socket.Connect(localEndPoint);
            }
            catch
            {
                Console.Write("Unable to connect !");
            }
            byte[] clientData = new byte[1024];
            int receivedBytesLen = socket.Receive(clientData);
            string file = Encoding.ASCII.GetString(clientData);
            char [] fileLength =new char [4];
            file.CopyTo(0, fileLength,0,4);         
            int length = int.Parse( new string (fileLength));
            Console.WriteLine("file name Length: " + length);
            char[] fileName = new char[length];
            file.CopyTo(4, fileName, 0,length);
            char[] fileContent = new char[file.Length - 4 - length];
            file.CopyTo(4 + length, fileContent, 0, file.Length - 4 - length);
            Console.WriteLine("File " + new string(fileName) + " Created !");// +fileName +
            Console.WriteLine("File Content: \n" + new string (fileContent));// +fileName +
            System.IO.File.WriteAllText(new string(fileName), new string(fileContent));
            Console.Read();
            socket.Close();
        }
    }
}
