﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Socket newServer = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            newServer.Bind(new IPEndPoint(IPAddress.Any, 8000));
            newServer.Listen(5);
            Console.Write("Enter file name to send to client:");
            String fileName = Console.ReadLine();
            Console.WriteLine("Mahmoud Atef Desouki Ahmed \n IS \n 5");
            String fileContent = System.IO.File.ReadAllText("test.txt");
            String concateString="";
            concateString = fileName.Length.ToString().PadLeft(4, '0');
            Socket clientSock = newServer.Accept();
            IPEndPoint localIpEndPoint = clientSock.LocalEndPoint as IPEndPoint;
            Console.WriteLine("Listening at IP :" + localIpEndPoint.Address + ",Port : " + localIpEndPoint.Port);
            Console.WriteLine("Client Accept");
            char[] Copystring = new char[fileContent.Length + 4 + fileName.Length];
            concateString.CopyTo(0, Copystring, 0, 4);
            fileName.CopyTo(0, Copystring, 4, fileName.Length);
            fileContent.CopyTo(0, Copystring, 4+fileName.Length, fileContent.Length);
            byte[] messageByteArray = Encoding.ASCII.GetBytes(Copystring);
            Console.WriteLine("Sending File :" + fileName);
            clientSock.Send(messageByteArray);
            Console.WriteLine("File Sent !");
            Console.Read();
            clientSock.Close();
        }
    }
}